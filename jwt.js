const requestJson = require('request-json');
var jwt = require('jsonwebtoken');
var bodyParser = require('body-parser');
//Declaro la variable con la que se generará y descifrará el token
const key= process.env.SECRET_KEY;

/*Creo una función a la cual se debe pasar el dato para validar y generar el token
 (en nuestro caso el email) y
devuelve el token de autenticación que luego se debe pasar en la cabecera tras el login.
*/
function createToken(email){
  var tokenData = {
     id: email
   };
   var token = jwt.sign(tokenData, key, {
      expiresIn: 60 * 60 * 1 // expira en media hora
   });

   return token;
 }

//Creo una función a la cual se debe pasar el token y devuelve un objeto respuesta con el mensaje a devolver por el servicio
//y el estado de la respuesta de la petición
 function verifyToken (token){
   if(!token){
      var result= {
         message: "Es necesario el token de autenticación",
         status: 401
       }
       console.log(JSON.stringify(result));
       var estado = 401;
       return result;
   }else{
     token = token.replace('Bearer ', '');

     var resultado = jwt.verify(token,key, function(err, email) {
       if (err) {
         var result= {
            message: 'Token inválido',
            status: 401
         }
         console.log("Caso 2: " + JSON.stringify(result));
         var estado = 401;
         return result;
       } else {
         //Si la respuesta es válida, ya no devuelvo el status porque sigue la ejecución en el servicio que la invoca y devolverá otro status.
         var result= {
            message: 'Token validado',
            status: 200
         }
         console.log("Caso 3: " + JSON.stringify(result));
         var estado = 200;
         return result;
       }
     })
   }
   return resultado;

 }

module.exports.createToken= createToken;
module.exports.verifyToken= verifyToken;
