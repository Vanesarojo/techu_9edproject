const bcrypt = require('bcrypt');

function hash(data){
    console.log("Hashing data");
    var hashedData= bcrypt.hashSync(data, 10);
    console.log(hashedData)
    return hashedData; //esta no tiene funciñon manejadora porque es síncrona
}
//Función para el login
function checkpassword(passwdordPlainText, passwdordFromDBHashed){
    console.log("Checking password");
    var checked = bcrypt.compareSync(passwdordPlainText, passwdordFromDBHashed);
    console.log(checked);
    return checked;
}


module.exports.hash = hash;
module.exports.checkpassword = checkpassword;
